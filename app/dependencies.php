<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([

        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new \Woke\WokeUidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        Capsule::class => function (ContainerInterface $c) {
            $capsule = new Capsule();

            $db_settings = $c->get('settings')['databases'];

            foreach ($db_settings as $key=> $values) {
                $capsule->addConnection($values, $key);
            }

            $capsule->setEventDispatcher(new Dispatcher(new Container()));

            // Make this Capsule instance available globally via static methods... (optional)
            $capsule->setAsGlobal();

            // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
            $capsule->bootEloquent();

            return $capsule;
        },
        // 普通url 的cache 用yac 不带锁的
        \Woke\Compents\IUrlCache::class => function (ContainerInterface $c) {
            return  new \Woke\Adapter\Cache\YacAdapter();
        },

        \Woke\Compents\StorgeCacheInterface::class => function (ContainerInterface $c) {
            $redis = $c->get('settings')['redis']['default'];

            return new \Woke\Adapter\Cache\RedisAdapter($redis['redis_host'], $redis['redis_port'], $redis['redis_auth'], $redis['redis_db']);
        },

    ]);
};
