<?php

declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

return function (App $app) {
//    $app->options('/{routes:.*}', function (Request $request, Response $response) {
    //        // CORS Pre-Flight OPTIONS Request Handler
    //        return $response;
    //    });

    $app->get('/phpinfo', function (Request $request, Response $response) {
        phpinfo();
        exit;
        $response->getBody()->write('Hello world!');

        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('{"code":200,"data":{}}');

        return $response;
    });

    $router = require __DIR__.'/routes/demo.php';

    $router($app);
};
