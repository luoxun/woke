<?php

declare(strict_types=1);

use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (\Slim\App $app) {
    $app->group('/demo', function (Group $group) {
        $group->get('', \App\Application\Actions\Demo\UserDemoAction::class)->addttl(60);
        //$group->post('',\App\Application\Actions\Demo\UserCreateAction::class);
    });
};
