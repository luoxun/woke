<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    $now = \Carbon\Carbon::now();

    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => true, // Should be set to false in production
            'logger'              => [
                'name'  => Yaconf::get('app.APP_NAME'),
                'path'  => isset($_ENV['docker']) ? 'php://stdout' : __DIR__.'/../logs/'.$now->year.'/'.$now->month.'/app_'.$now->day.'.log',
                'level' => Logger::DEBUG,
            ],

            'databases' => [
                'default' => [
                    'driver'    => 'mysql',
                    'host'      => Yaconf::get('db.DB_HOST'),
                    'database'  => Yaconf::get('db.DB_DATABASE'),
                    'username'  => Yaconf::get('db.DB_USERNAME'),
                    'password'  => Yaconf::get('db.DB_PASSWORD'),
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
                ],
            ],

            'redis' => [
                'default' => [
                    'redis_host'      => Yaconf::get('redis.REDIDS_HOST'),
                    'redis_port'      => 6379,
                    'redis_db'        => Yaconf::get('redis.REDIS_DB'),
                    'redis_auth'      => Yaconf::get('redis.REDIS_AUTH'),
                ],
            ],

        ],
    ]);
};
