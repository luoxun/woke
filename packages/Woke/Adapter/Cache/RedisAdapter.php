<?php

namespace Woke\Adapter\Cache;

use Woke\Compents\StorgeCacheInterface;

class RedisAdapter implements StorgeCacheInterface
{
    private $item;

    public function __construct($host, $port, $auth, $db)
    {

        //var_dump($host,$port,$auth,$db);

        $this->item = new \Redis();
        $this->item->connect($host, $port);

        if (!empty($auth)) {
            $this->item->auth($auth);
        }

        $this->item->select($db);
    }

    public function get(string $key): object
    {
        return unserialize($this->item->get($key));
    }

    public function set(string $key, $value, int $ttl = 30): string
    {
        return $this->item->set($key, serialize($value), $ttl);
    }
}
