<?php

namespace Woke\Adapter\Cache;

use Woke\Compents\IUrlCache;

class YacAdapter implements IUrlCache
{
    private $item;

    public function __construct()
    {
        $this->item = new \Yac();
    }

    public function get(string $key): string
    {
        return $this->item->get($key);
    }

    public function set(string $key, string $value, int $ttl): string
    {
        return $this->item->set($key, $value, $ttl);
    }
}
