<?php

declare(strict_types=1);

namespace Woke\Compents;

interface IUrlCache
{
    public function get(string $key): string;

    public function set(string $key, string $value, int $ttl): string;
}
