<?php

namespace Woke\Compents;

interface StorgeCacheInterface
{
    public function get(string $key);

    public function set(string $key, string $value, int $ttl = 100);
}
