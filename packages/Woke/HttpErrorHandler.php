<?php

declare(strict_types=1);

namespace Woke;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpException;
use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpMethodNotAllowedException;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpNotImplementedException;
use Slim\Exception\HttpUnauthorizedException;
use Slim\Handlers\ErrorHandler as SlimErrorHandler;
use Throwable;
use Woke\Exceptions\WokeExceptions;
use Woke\Exceptions\WokeValidatorException;

class HttpErrorHandler extends SlimErrorHandler
{
    /**
     * {@inheritdoc}
     */
    protected function respond(): Response
    {
        $exception = $this->exception;
        //$statusCode = 500;

        $statusCode = $this->statusCode;

        $boolHiddenStatus = true;

        $error = new ActionError(
            ActionError::SERVER_ERROR,
            'An internal error has occurred while processing your request.'
        );
        if ($exception instanceof  WokeValidatorException) {
            $statusCode = $exception->getCode();
            $error->setType('validator ERROR');

            $error->setDescription($exception->getMessage());
        //$error->setDescription($exception->getMessage());
        } elseif ($exception instanceof  WokeExceptions) {
            $statusCode = $exception->getCode();
            $error->setType('BIZ ERROR');
        }

        //var_dump($exception);exit;

        if ($exception instanceof HttpException) {
            $statusCode = $exception->getCode();

            //$statusCode = 200;
            $error->setDescription($exception->getMessage());

            $boolHiddenStatus = false;

            if ($exception instanceof HttpNotFoundException) {
                $error->setType(ActionError::RESOURCE_NOT_FOUND);
            } elseif ($exception instanceof HttpMethodNotAllowedException) {
                $statusCode = 405;
                $error->setType(ActionError::NOT_ALLOWED);
            } elseif ($exception instanceof HttpUnauthorizedException) {
                $error->setType(ActionError::UNAUTHENTICATED);
            } elseif ($exception instanceof HttpForbiddenException) {
                $error->setType(ActionError::INSUFFICIENT_PRIVILEGES);
            } elseif ($exception instanceof HttpBadRequestException) {
                $error->setType(ActionError::BAD_REQUEST);
            } elseif ($exception instanceof HttpNotImplementedException) {
                $error->setType(ActionError::NOT_IMPLEMENTED);
            }
        }

        if (
            !($exception instanceof HttpException)
            && ($exception instanceof Exception || $exception instanceof Throwable)
            && $this->displayErrorDetails
        ) {
            $error->setDescription($error->getDescription());
        }

        $payload = new ActionPayload($statusCode, null, $error);
        $encodedPayload = json_encode($payload, JSON_PRETTY_PRINT);
        // aways return 200
        $statusCode = 200;
//        if ($boolHiddenStatus === true) {
//            $response = $this->responseFactory->createResponse(200);
//        } else {
        $response = $this->responseFactory->createResponse($statusCode);
        //       }

        //       var_dump($encodedPayload);exit;
        $response->getBody()->write($encodedPayload);

        return $response->withHeader('Content-Type', 'application/json');
    }
}
