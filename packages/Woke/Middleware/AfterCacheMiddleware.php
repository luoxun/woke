<?php

//declare(strict_types=1);

namespace Woke\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\App;
use Woke\Compents\IUrlCache;

class AfterCacheMiddleware
{
    private $ttl;

    private $app;

    public function __construct(int $ttl, IUrlCache $app)
    {
        $this->ttl = $ttl;

        $this->app = $app;
    }

    public function __invoke(Request $request, RequestHandler $handler): Response
    {
        $response = $handler->handle($request);

        if ($response->getStatusCode() == 200) {
            //$cache = $this->app->getContainer()->get(IUrlCache::class);

            $cache = $this->app;

            $cache->set($request->getRequestTarget(), sprintf($response->getBody()), $this->ttl);
        }

        return $response;
    }
}
