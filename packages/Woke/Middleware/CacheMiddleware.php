<?php

declare(strict_types=1);

namespace Woke\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;
use Woke\Compents\IUrlCache;

/**
 * Class CacheMiddleware.
 */
class CacheMiddleware implements MiddlewareInterface
{
    private $cache;

    public function __construct(IUrlCache $cache)
    {
        $this->cache = $cache;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // TODO: Implement process() method.

        // 如果head 头标明 no-cache 则 直接放过去
        if ($request->hasHeader('Cache-Control') and 'no-cache' == $request->getHeader('Cache-Control')[0]) {
            return $handler->handle($request);
        }

        if (!empty($data = $this->cache->get($request->getRequestTarget()))) {
            $response = new Response();
            $response->getBody()->write($data);

            return $response->withHeader('Yac-Cache', 'true');
        }

        return $handler->handle($request);
    }
}
