<?php

namespace Woke;

use InvalidArgumentException;
use Slim\Psr7\Response;

class WokeResponse extends Response
{
    /**
     * Filter HTTP status code.
     *
     * @param int $status HTTP status code.
     *
     * @throws InvalidArgumentException If an invalid HTTP status code is provided.
     *
     * @return int
     */
    protected function filterStatus($status): int
    {
//        if (!is_integer($status) || $status < StatusCodeInterface::STATUS_CONTINUE || $status > 599) {
//            throw new InvalidArgumentException('Invalid HTTP status code.');
//        }

        return $status;
    }
}
