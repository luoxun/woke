<?php

namespace Woke;

use Slim\Routing\Route;
use Woke\Compents\IUrlCache;
use Woke\Middleware\AfterCacheMiddleware;

class WokeRoute extends Route implements WokeRouteInterface
{
    public function addTtl(int $ttl)
    {
        // TODO: Implement addttl() method.

        // var_dump($this->container->get(IUrlCache::class));

        // $ttl = 100;

        $this->middlewareDispatcher->add(new AfterCacheMiddleware($ttl, $this->container->get(IUrlCache::class)));

        return $this;
    }
}
