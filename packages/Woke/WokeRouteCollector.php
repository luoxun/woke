<?php

namespace Woke;

use Slim\Interfaces\RouteInterface;
use Slim\Routing\RouteCollector;

class WokeRouteCollector extends RouteCollector
{
    /**
     * @param string[]        $methods
     * @param string          $pattern
     * @param callable|string $callable
     *
     * @return RouteInterface
     */
    protected function createRoute(array $methods, string $pattern, $callable): RouteInterface
    {
        return new WokeRoute(
            $methods,
            $pattern,
            $callable,
            $this->responseFactory,
            $this->callableResolver,
            $this->container,
            $this->defaultInvocationStrategy,
            $this->routeGroups,
            $this->routeCounter
        );
    }
}
