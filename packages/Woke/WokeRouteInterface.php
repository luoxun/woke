<?php

namespace Woke;

use Slim\Interfaces\RouteInterface;

interface WokeRouteInterface extends RouteInterface
{
    public function addTtl(int $ttl);
}
