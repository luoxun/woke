<?php

namespace Woke;

use Monolog\Processor\UidProcessor;

class WokeUidProcessor extends UidProcessor
{
    private $uid;

    public function __construct(int $length = 7)
    {
        if ($length > 32 || $length < 1) {
            throw new \InvalidArgumentException('The uid length must be an integer between 1 and 32');
        }

        $this->uid = $this->generateUid($length);
    }

    public function __invoke(array $record): array
    {
        $record['extra']['request_id'] = $this->uid;

        return $record;
    }

    private function generateUid(int $length): string
    {
        return substr(bin2hex(random_bytes((int) ceil($length / 2))), 0, $length);
    }
}
