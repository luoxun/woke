<?php

namespace Woke;

use Woke\Exceptions\WokeValidatorException;
use Woke\Validation\WokeValidatorFactory;

if (!function_exists('Woke\WokeValidator')) {
    function WokeValidator($data, $rule)
    {
        $val = (new WokeValidatorFactory())->make($data, $rule);

        if ($val->passes() === false) {

//            var_dump((($val->errors()->jsonSerialize())));
//            exit;
            throw  new WokeValidatorException(array_keys($val->errors()->jsonSerialize())[0]);
        }
    }
}
