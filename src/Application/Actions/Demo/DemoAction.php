<?php

namespace App\Application\Actions\Demo;

use App\Domain\User\UserRepository;
use Illuminate\Database\Capsule\Manager;
use Psr\Log\LoggerInterface;
use Woke\Action;
use Woke\Compents\StorgeCacheInterface;

abstract class DemoAction extends Action
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(LoggerInterface $logger, Manager $capsule, StorgeCacheInterface $storgeCache, UserRepository $userRepository)
    {
        parent::__construct($logger, $capsule, $storgeCache);
        $this->userRepository = $userRepository;
    }
}
