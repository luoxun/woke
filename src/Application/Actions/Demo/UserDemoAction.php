<?php

namespace App\Application\Actions\Demo;

use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Woke\Validation\WokeValidatorFactory;
use function Woke\WokeValidator;

class UserDemoAction extends DemoAction
{
    /**
     * @throws DomainRecordNotFoundException
     */
    protected function action(): Response
    {
        $this->cache->set('google', $this->userRepository->findUserOfId(4));

        $user = $this->cache->get('google');

        $postdata = [
            'title' => 1000,
        ];

        $rule = [
            'title' => 'Bail|required|gt:10000|alpha_num',
            'body'  => 'bail|required|gte:10000|alpha_num',
        ];

        WokeValidator($postdata, $rule);

        //$this->demo([], []);
        // $val = (new WokeValidatorFactory())->make(
        //     [
        //         'title' => 1000,
        //         //'body' => 1,
        //     ], [
        //         'title' => 'bail|required|gt:10000|alpha_num',
        //         // 'body' => 'bail|required|gte:10000|alpha_num',
        //     ]
        // );

        // var_dump($val->passes());

        // var_dump($val->errors());exit;

        // exit;

//        $val->addRules(
//            [
//                'title' => 'required|unique:posts|max:255',
//                'body' => 'required',
//            ]
//        );
//
//        $val->validate();
//
//        exit;

        return $this->respondWithData($user)->withHeader('Yac-Cache', 'false');
    }

    public function demo($data, $rule)
    {
        $val = (new WokeValidatorFactory())->make(
            [
                'title' => 1000,
                'body'  => 1,
            ],
            [
                'title' => 'bail|required|gt:10000|alpha_num',
                'body'  => 'bail|required|gte:10000|alpha_num',
            ]
        );

        var_dump($val->passes());

        if ($val->passes() === false) {
            var_dump($val->errors()->toJson());
            exit;
        }

        var_dump($val->errors());
        exit;

        exit;
    }
}
