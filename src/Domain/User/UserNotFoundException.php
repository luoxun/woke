<?php

declare(strict_types=1);

namespace App\Domain\User;

use Woke\Exceptions\WokeExceptions;

class UserNotFoundException extends WokeExceptions
{
    protected $code = 200404;

    public $message = 'The user you requesteddddd does not exist.';
}
