<?php

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use App\Models\UsersModel;

class DemoUserRepository implements UserRepository
{
    /**
     * @return User[]
     */
    public function findAll(): array
    {
        // TODO: Implement findAll() method.
    }

    /**
     * @param int $id
     *
     * @throws UserNotFoundException
     *
     * @return User
     */
    public function findUserOfId(int $id): UsersModel
    {
        $result = UsersModel::find($id);

        if (empty($result)) {
            throw  new UserNotFoundException();
        }

        return  $result;
    }
}
